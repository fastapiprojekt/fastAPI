"""create posts table

Revision ID: b243a0d27157
Revises: 
Create Date: 2023-03-09 12:13:50.745186

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = 'b243a0d27157'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table( 'posts', sa.Column( 'id', sa.Integer(), nullable=False,
                                         primary_key=True ), sa.Column( 'title', sa.String(), nullable=False ) )
    pass


def downgrade():
    op.drop_table( 'posts' )
    pass
