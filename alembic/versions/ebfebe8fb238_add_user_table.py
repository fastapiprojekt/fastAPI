"""add user table

Revision ID: ebfebe8fb238
Revises: d7f6e47f90a3
Create Date: 2023-03-09 12:50:28.673149

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = 'ebfebe8fb238'
down_revision = 'd7f6e47f90a3'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table( 'users',
                     sa.Column( 'id', sa.Integer(), nullable=False ),
                     sa.Column( 'email', sa.String(), nullable=False ),
                     sa.Column( 'password', sa.String(), nullable=False ),
                     sa.Column( 'created_at', sa.TIMESTAMP( timezone=True ),
                                server_default=sa.text( 'now()' ), nullable=False ),
                     sa.PrimaryKeyConstraint( 'id' ),
                     sa.UniqueConstraint( 'email' )
                     )
    pass


def downgrade():
    op.drop_table( 'users' )
    pass
