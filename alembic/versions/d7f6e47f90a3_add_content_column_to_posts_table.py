"""add content column to posts table

Revision ID: d7f6e47f90a3
Revises: b243a0d27157
Create Date: 2023-03-09 12:38:03.562971

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = 'd7f6e47f90a3'
down_revision = 'b243a0d27157'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column( 'posts', sa.Column( 'content', sa.String(), nullable=False ) )
    pass


def downgrade():
    op.drop_column( 'posts', 'content' )
    pass
