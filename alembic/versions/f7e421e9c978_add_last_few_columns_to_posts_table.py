"""add last few columns to posts table

Revision ID: f7e421e9c978
Revises: ab66468547b1
Create Date: 2023-03-09 13:07:37.449170

"""
import sqlalchemy as sa

from alembic import op

# revision identifiers, used by Alembic.
revision = 'f7e421e9c978'
down_revision = 'ab66468547b1'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column( 'posts', sa.Column(
        'published', sa.Boolean(), nullable=False, server_default='TRUE' ), )
    op.add_column( 'posts', sa.Column(
        'created_at', sa.TIMESTAMP( timezone=True ), nullable=False, server_default=sa.text( 'NOW()' ) ), )
    pass


def downgrade():
    op.drop_column( 'posts', 'published' )
    op.drop_column( 'posts', 'created_at' )
    pass
